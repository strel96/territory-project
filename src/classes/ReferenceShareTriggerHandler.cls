public with sharing class ReferenceShareTriggerHandler {
    private DataService dataService = new DataService();
    private SharingService sharingService = new SharingService();

    public void createSharing(List<Reference__c> references){
        Set<Id> contactsId = new Set<Id>();
        Set<Id> accountsId = new Set<Id>();

        for(Reference__c ref : references){
            if(ref.Account__c != null) {
                accountsId.add(ref.Account__c);
            }
            if(ref.Contact__c != null){
                contactsId.add(ref.Contact__c);
            }
        }
        Set<Id> accountsTerritoryId = dataService.getAccountsTerritoryId(accountsId);

        Set<Id> parentTerritoriesId = dataService.getParentTerritoriesId(accountsTerritoryId);
        accountsTerritoryId.addAll(parentTerritoriesId);

        Set<Id> usersId = dataService.getUsersId(accountsTerritoryId);

        sharingService.insertContactSharing(contactsId, usersId);
    } 
    
    public void deleteSharing(List<Reference__c> references){
        Set<Id> contactsId = new Set<Id>();
        Set<Id> accountsId = new Set<Id>();

        for(Reference__c ref : references){
            if(ref.Account__c != null) {
                accountsId.add(ref.Account__c);
            }
            if(ref.Contact__c != null){
                contactsId.add(ref.Contact__c);
            }
        }
        Set<Id> accountsTerritoryId = dataService.getAccountsTerritoryId(accountsId);

        Set<Id> parentTerritoriesId = dataService.getParentTerritoriesId(accountsTerritoryId);
        accountsTerritoryId.addAll(parentTerritoriesId);

        Set<Id> usersId = dataService.getUsersId(accountsTerritoryId);

        sharingService.deleteContactSharing(contactsId, usersId);
    }
}