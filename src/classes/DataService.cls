public with sharing class DataService {
    public Set<Id> getAccountsId(Set<Id> territoriesId){
       	List<Account> accounts;
        Set<Id> result = new Set<Id>();
        
        if(territoriesId != null){
            accounts = [
                SELECT Id, Territory__c 
                FROM Account 
                WHERE Territory__c IN :territoriesId
            ];
        }
        
        if(accounts != null){
            for(Account acc : accounts){
                result.add(acc.Id);
            }
        }
        
        return result;
    }
    
    public Set<Id> getContactsId(Set<Id> accountsId){
       	List<Reference__c> contacts;
        Set<Id> result = new Set<Id>();

        if(accountsId != null){
            contacts = [
                SELECT Contact__c 
                FROM Reference__c
                WHERE Account__c IN : accountsId
            ];
        }
        
        if(contacts != null) {
            for (Reference__c con : contacts) {
                result.add(con.Contact__c);
            }
        }
        
        return result;
    }
    
    public Set<Id> getAccountsTerritoryId(Set<Id> accountsId){
        List<Account> accounts;
        Set<Id> result = new Set<Id>();

        if(accountsId != null){
            accounts = [
                SELECT Territory__c 
                FROM Account
                WHERE Id IN :accountsId];
        }
        
        if(accounts != null){
            for(Account acc : accounts){
                result.add(acc.Territory__c);
            }
        }
        
        return result;
    }

    public Set<Id> getChildTerritoriesId(Set<Id> territoriesId){
       	List<Territory__c> childTerritories;
        Set<Id> result = new Set<Id>();
        
        if(territoriesId != null){
            childTerritories = [
                SELECT Id
                FROM Territory__c 
                WHERE Id IN :territoriesId
                OR Territory__c IN :territoriesId 
                OR Territory__r.Territory__c IN :territoriesId
                OR Territory__r.Territory__r.Territory__c IN :territoriesId
                OR Territory__r.Territory__r.Territory__r.Territory__c IN :territoriesId
                OR Territory__r.Territory__r.Territory__r.Territory__r.Territory__c IN :territoriesId
            ];
        }
        
        if(territoriesId != null){
            for(Territory__c terr : childTerritories){
                result.add(terr.Id);
            }
        }
        
        return result;
    }
    
    public Set<Id> getParentTerritoriesId(Set<Id> territoriesId){
       	List<Territory__c> parentTerritories;
        Set<Id> result = new Set<Id>();
        
        if(territoriesId != null){
           parentTerritories = [
               SELECT Territory__c
               		, Territory__r.Territory__c
                    , Territory__r.Territory__r.Territory__c
                    , Territory__r.Territory__r.Territory__r.Territory__c
                    , Territory__r.Territory__r.Territory__r.Territory__r.Territory__c
                    , Territory__r.Territory__r.Territory__r.Territory__r.Territory__r.Territory__c
               	  FROM Territory__c
                  WHERE Id IN :territoriesId
           ];
       }
        
        for(Territory__c terr : parentTerritories) {
            result.add(terr.Territory__c);
            result.add(terr.Territory__r.Territory__c);
            result.add(terr.Territory__r.Territory__r.Territory__c);
            result.add(terr.Territory__r.Territory__r.Territory__r.Territory__c);
            result.add(terr.Territory__r.Territory__r.Territory__r.Territory__r.Territory__c);
            result.add(terr.Territory__r.Territory__r.Territory__r.Territory__r.Territory__r.Territory__c);
        }
        
        return result;
    }
    
    public Set<Id> getUsersId(Set<Id> territoriesId){
       	List<TerrUser__c> terrUsers;
        Set<Id> result = new Set<Id>();
        
        if(territoriesId != null){
            terrUsers = [
                SELECT User__c, Territory__c 
                FROM TerrUser__c 
                WHERE Territory__c IN :territoriesId
            ];
        }
        
        if(terrUsers != null){
            for(TerrUser__c terr : terrUsers){
                result.add(terr.User__c);
            }
        }
        
        return result;
    }
    
    public List<TerrUser__c> getUsersTerritoriesId(Set<Id> usersId){
       	List<TerrUser__c> usersTerr;
        
        if(!usersId.isEmpty()){
            usersTerr = [
                SELECT User__c, Territory__c 
                FROM TerrUser__c 
                WHERE User__c IN :usersId
            ];
        }
        
        return usersTerr;
    }
}