@IsTest
public class TerritoryTestFactory {

    public static Set<Id> getUsersIds(){
        List<User> users = [SELECT Id FROM User WHERE Id = '0052X000009uwYyQAI'];
        Set<Id> usersIds = new Set<Id>();

        for(User user : users){
            usersIds.add(user.Id);
        }

        return usersIds;
    }

    public static Set<Id> getAccountsIds(Integer quantity){
        List<Account> accounts = getAccounts(quantity);
        Set<Id> accountsIds = new Set<Id>();

        for(Account account : accounts){
            accountsIds.add(account.Id);
        }

        return accountsIds;
    }

    public static Set<Id> getContactsIds(Integer quantity){
        List<Contact> contacts = getContacts(quantity);
        Set<Id> contactsIds = new Set<Id>();


        for(Contact contact : contacts){
            contactsIds.add(contact.Id);
        }

        return contactsIds;
    }

    public static Set<Id> getTerritoriesIds(Integer quantity){
        List<Territory__c> territories = getTerritories(quantity);
        Set<Id> territoriesIds = new Set<Id>();

        for(Territory__c territory : territories){
            territoriesIds.add(territory.Id);
        }

        return territoriesIds;
    }

    public static Account getAccount(Boolean doInsert){
        Account account = new Account(Name = 'Account Test');

        if(doInsert) insert account;

        return account;
    }

    public static Contact getContact(Boolean doInsert){
        Contact contact = new Contact(LastName = 'Contact Test');

        if(doInsert) insert contact;

        return contact;
    }

    public static List<Account> getAccounts(Integer quantity){
        List<Account> accounts = new List<Account>();

        for (Integer i = 0; i < quantity; i++) {
            accounts.add(getAccount(false));
        }

        insert accounts;

        return accounts;
    }

    public static List<Contact> getContacts(Integer quantity){
        List<Contact> contacts = new List<Contact>();

        for (Integer i = 0; i < quantity; i++) {
            contacts.add(getContact(false));
        }

        insert contacts;

        return contacts;
    }


    public static Territory__c getTerritory(Boolean doInsert){
        Territory__c territory = new Territory__c(Name = 'Test');

        if(doInsert) insert territory;

        return territory;
    }

    public static Reference__c getReference(Boolean doInsert){
        Reference__c reference = new Reference__c(Name = 'Test');

        if(doInsert) insert reference;

        return reference;
    }


    public static List<Territory__c> getTerritories(Integer terrQuantity){
        List<Territory__c> territories = new List<Territory__c>();

        for (Integer i = 0; i < terrQuantity; i++) {
            territories.add(getTerritory(false));
        }

        insert territories;

        return territories;
    }

    public static List<Reference__c> getReferences(Integer refQuantity){
        List<Reference__c> references = new List<Reference__c>();

        for (Integer i = 0; i < refQuantity; i++) {
            references.add(getReference(false));
        }

        insert references;

        return references;
    }

    public static List<Account> generateAccountsWithTerritories(Integer accQuantity){
        List<Account> accounts = new List<Account>();

        for(Integer i = 0; i < accQuantity; i++){
            Account account = new Account(Name = 'Test ' + i);
            insert account;
            accounts.add(account);
        }

        for(Account account : accounts){
            account.Territory__c = getTerritory(true).Id;
            update account;
        }


        return accounts;
    }

    public static List<Reference__c> generateAccountsAndContactsForReferences(Integer refQuantity){
        List<Reference__c> references = new List<Reference__c>();
        List<Account> accounts = getAccounts(refQuantity);
        List<Contact> contacts = getContacts(refQuantity);

        for(Integer i = 0; i < refQuantity; i++){
            Reference__c reference = getReference(false);
            reference.Account__c = accounts[i].Id;
            reference.Contact__c = contacts[i].Id;
            references.add(reference);

        }

        insert references;

        return references;
    }

    public static List<Territory__c> generateChildTerritories(Integer quantity){
//        Territory__c territory = getTerritory(true);
//        territory.Territory__c = getTerritory(true).Id;
//        territory.Territory__r.Territory__c = getTerritory(true).Id;
//        territory.Territory__r.Territory__r.Territory__c = getTerritory(true).Id;
//
//        insert territory;

        List<Territory__c> territories = getTerritories(quantity);

        for(Integer i = 0; i < territories.size(); i++){
            if(i != territories.size() - 1) {
                territories[i].Territory__c = territories[i+1].Id;
            }
        }

        update territories;

        return territories;
    }

    public static User generateUser(String profileName){
        UserRole userRole = new UserRole(DeveloperName = 'TestingTeam', Name = 'Testing Team');
        insert userRole;
        User user = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = :profileName].Id,
                LastName = 'last',
                Email = 'Cpt.Awesome@awesomesauce.com',
                Username = 'Cpt.Awesome@awesomesauce.com',
                CompanyName = 'Testing Co',
                Title = 'Capitan',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                UserRoleId = userRole.Id
        );
        insert user;

        return user;
    }

    public static List<TerrUser__c> generateTerrUser(Integer terrQuantity){
        List<TerrUser__c> terrUsers = new List<TerrUser__c>();
        Territory__c territory = getTerritory(true);
        User user = generateUser('System Administrator');

//        for(Integer i = 0; i < terrQuantity; i++){
//            TerrUser__c terrUser = new TerrUser__c();
//            terrUser.Territory__c = territory.Id;
//            terrUser.User__c = user.Id;
//            terrUsers.add(terrUser);
//        }
//        insert terrUsers;

        return terrUsers;
    }

}