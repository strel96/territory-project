public with sharing class ContactShareTriggerHandler {
    public static void afterInsertOrUpdateSharing(List<Contact> contacts){
        List<ContactShare> contactSharing = new List<ContactShare>();
        List<Reference__c> references = [
            SELECT Account__r.Territory__c, Contact__c FROM Reference__c WHERE Contact__c IN :contacts];
        Set<Id> territoriesId = new Set<Id>();
        Set<Id> contactsId = new Set<Id>();
        
        for(Reference__c ref: references){
            territoriesId.add(ref.Account__r.Territory__c);
        }
        for(Contact con : contacts){
            contactsId.add(con.id);
        }
        
        List<TerrUser__c> users = [SELECT User__c, Territory__c FROM TerrUser__c WHERE Territory__c IN :territoriesId];
        List<ContactShare> readySharing = [SELECT UserOrGroupId, ContactId FROM ContactShare WHERE ContactId IN : contactsId];
        
        for(Reference__c ref : references){
            for(TerrUser__c user : users){
                if(ref.Account__r.Territory__c == user.Territory__c){
                    ContactShare share = getContactSharing(user.User__c, ref.Contact__c, readySharing);
                    if(share == null){
                        ContactShare newShare = new ContactShare();
                        newShare.UserOrGroupId = user.User__c;
                        newShare.ContactId = ref.Contact__c;
                        newShare.ContactAccessLevel = 'Edit';
                        contactSharing.add(newShare);
                    }
                }
            }
        }
          upsert contactSharing;
        
    }
    
    private static ContactShare getContactSharing(Id userId, Id contactId, List<ContactShare> shares){
        ContactShare share;
        for(ContactShare item : shares){
            if(item.UserOrGroupId == userId && item.ContactId == contactId){
                share = item;
            }
        }
        return share;
    }

}