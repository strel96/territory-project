@IsTest
public class DataServiceTest {
    private static DataService dataService = new DataService();

    @IsTest public static void getAccountsIdsTest(){
        List<Account> accounts = TerritoryTestFactory.generateAccountsWithTerritories(2);
        Set<Id> territoriesIds = new Set<Id>();
        Set<Id> accountsIds = new Set<Id>();

        for(Account account : accounts){
            territoriesIds.add(account.Territory__c);
        }

        Test.startTest();
        accountsIds = dataService.getAccountsId(territoriesIds);
        Test.stopTest();

        System.assert(!accountsIds.isEmpty(), 'Error');
        System.assertEquals(2, accountsIds.size(), 'Error');
    }

    @IsTest public static void getContactsIdsTest(){
        List<Reference__c> references = TerritoryTestFactory.generateAccountsAndContactsForReferences(2);
        Set<Id> accountsIds = new Set<Id>();
        Set<Id> contactsIds = new Set<Id>();

        for(Reference__c reference : references){
            accountsIds.add(reference.Account__c);
        }

        Test.startTest();
        contactsIds = dataService.getContactsId(accountsIds);
        Test.stopTest();

        System.assert(!contactsIds.isEmpty(), 'Error');
        System.assertEquals(2, contactsIds.size(), 'Error');
    }

    @IsTest public static void getAccountsTerritoryIdTest(){
        List<Account> accounts = TerritoryTestFactory.generateAccountsWithTerritories(5);
        Set<Id> accountsIds = new Set<Id>();
        Set<Id> territoriesIds = new Set<Id>();

        for(Account account : accounts){
            accountsIds.add(account.Id);
        }

        Test.startTest();
        territoriesIds = dataService.getAccountsTerritoryId(accountsIds);
        Test.stopTest();

        System.assert(!territoriesIds.isEmpty());
        System.assertEquals(5, territoriesIds.size());
    }

    @IsTest public static void getChildAndParentTerritoriesIdsTest(){
        List<Territory__c> territories = TerritoryTestFactory.generateChildTerritories(4);
        Set<Id> territoriesIds = new Set<Id>();

        for(Territory__c territory : territories) {
            territoriesIds.add(territory.Id);
        }

        Set<Id> childTerritoriesIds = new Set<Id>();
        Set<Id> parentTerritoriesIds = new Set<Id>();

        Test.startTest();
        childTerritoriesIds = dataService.getChildTerritoriesId(territoriesIds);
        parentTerritoriesIds = dataService.getParentTerritoriesId(territoriesIds);
        Test.stopTest();

        System.assert(!childTerritoriesIds.isEmpty());
        System.assertEquals(4, childTerritoriesIds.size());

        System.assert(!parentTerritoriesIds.isEmpty());
        System.assertEquals(4, parentTerritoriesIds.size());
    }

   /* @IsTest public static void getUsersAndTerritoriesIdsFromTerrUser(){
        List<TerrUser__c> terrUsers = TerritoryTestFactory.generateTerrUser(1);

//        User user = TerritoryTestFactory.generateUser('System Administrator');

//        System.assert(!terrUsers.isEmpty());

    }*/
}