public with sharing class AccountShareTriggerHandler {
    private DataService dataService = new DataService();
    private SharingService sharingService = new SharingService();

    public void createSharing(Map<Id, Account> accounts){
        Set<Id> accTerritoriesId = new Set<Id>();
        
        for(Account acc : accounts.values()){
            if(acc.Territory__c != null) {
                accTerritoriesId.add(acc.Territory__c);
            }
        }
        
        if(!accTerritoriesId.isEmpty()) {
            Set<Id> parentTerritories = dataService.getParentTerritoriesId(accTerritoriesId);
            accTerritoriesId.addAll(parentTerritories);
        }
        
        Set<Id> usersId = dataService.getUsersId(accTerritoriesId);
        Set<Id> contactsId = dataService.getContactsId(accounts.keySet());

        sharingService.insertAccountSharing(accounts.keySet(), usersId);
        sharingService.insertContactSharing(contactsId, usersId);
    }
    
    public void deleteSharing(Map<Id, Account> accounts){
        Set<Id> accTerritoriesId = new Set<Id>();

        for(Account acc: accounts.values()){
            if(acc.Territory__c != null) {
                accTerritoriesId.add(acc.Territory__c);
            }
        }
        
        if(!accTerritoriesId.isEmpty()) {
            Set<Id> parentTerritories = dataService.getParentTerritoriesId(accTerritoriesId);
            accTerritoriesId.addAll(parentTerritories);
        }
        
        Set<Id> usersId = dataService.getUsersId(accTerritoriesId);
        Set<Id> contactsId = dataService.getContactsId(accounts.keySet());

        sharingService.deleteAccountSharing(accounts.keySet(), usersId);
        sharingService.deleteContactSharing(contactsId, usersId);
    }


    /* public static void createSharing(List<Account> accounts){
        Set<Id> accTerritoriesId = new Set<Id>();
        Set<Id> accountsId = new Set<Id>();
        
        for(Account acc: accounts){
            if(acc.Territory__c != null) {
                accTerritoriesId.add(acc.Territory__c);
            }
            accountsId.add(acc.Id);
        }
        if(!accTerritoriesId.isEmpty()) {
            Set<Id> parentTerritories = DataService.getParentTerritoriesId(accTerritoriesId);
            accTerritoriesId.addAll(parentTerritories);
        }
        
        Set<Id> usersId = DataService.getUsersId(accTerritoriesId);
        Set<Id> contactsId = DataService.getContactsId(accountsId);
        
        SharingService.insertAccountSharing(accountsId, usersId);
        SharingService.insertContactSharing(contactsId, usersId);
    } 
    
    public static void deleteSharing(List<Account> accounts){
        Set<Id> accTerritoriesId = new Set<Id>();
        Set<Id> accountsId = new Set<Id>();

        for(Account acc: accounts){
            if(acc.Territory__c != null) {
                accTerritoriesId.add(acc.Territory__c);
            }
            accountsId.add(acc.Id);
        }
        
        if(!accTerritoriesId.isEmpty()) {
            Set<Id> parentTerritories = DataService.getParentTerritoriesId(accTerritoriesId);
            accTerritoriesId.addAll(parentTerritories);
        }
        
        Set<Id> usersId = DataService.getUsersId(accTerritoriesId);
        Set<Id> contactsId = DataService.getContactsId(accountsId);
		
        SharingService.deleteAccountSharing(accountsId, usersId);
        SharingService.deleteContactSharing(contactsId, usersId);
    }*/
}