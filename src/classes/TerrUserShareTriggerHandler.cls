public with sharing class TerrUserShareTriggerHandler {
    private DataService dataService = new DataService();
    private SharingService sharingService = new SharingService();

    public void createSharing(List<TerrUser__c> terrUsers) {
        Set<Id> usersId = new Set<Id>();
        Set<Id> userTerritoriesId = new Set<Id>();
        
        for (TerrUser__c terr : terrUsers) {
            if (terr.User__c != null) {
                usersId.add(terr.User__c);
            }
            if (terr.Territory__c != null) {
                userTerritoriesId.add(terr.Territory__c);
            }
        }
        
        if (!userTerritoriesId.isEmpty()) {
            Set<Id> childTerritoriesId = dataService.getChildTerritoriesId(userTerritoriesId);
            userTerritoriesId.addAll(childTerritoriesId);
        }
        
        Set<Id> accountsId = dataService.getAccountsId(userTerritoriesId);
        Set<Id> contactsId = dataService.getContactsId(accountsId);

        sharingService.insertAccountSharing(accountsId, usersId);
        sharingService.insertContactSharing(contactsId, usersId);
    }
    
    public void deleteSharing(List<TerrUser__c> terrUsers) {
        Set<Id> userTerritoriesId = new Set<Id>();
        Set<Id> usersId = new Set<Id>();
        
        for (TerrUser__c terr : terrUsers) {
            if (terr.User__c != null) {
                usersId.add(terr.User__c);
            }
            if (terr.Territory__c != null) {
                userTerritoriesId.add(terr.Territory__c);
            }
        }
        Set<Id> childTerritoriesId = dataService.getChildTerritoriesId(userTerritoriesId);
        userTerritoriesId.addAll(childTerritoriesId);
        
        Set<Id> accountsId = dataService.getAccountsId(userTerritoriesId);
        Set<Id> contactsId = dataService.getContactsId(accountsId);

        sharingService.deleteAccountSharing(accountsId, usersId);
        sharingService.deleteContactSharing(contactsId, usersId);
        
        if(!usersId.isEmpty()){
            List<TerrUser__c> usersTerritoriesId = dataService.getUsersTerritoriesId(usersId);
            
            if(usersTerritoriesId.size() >= 1){
                for(TerrUser__c terr : terrUsers){
                    Integer a = usersTerritoriesId.indexOf(terr);
                    if(a != -1) usersTerritoriesId.remove(a);
                }
                createSharing(usersTerritoriesId);
            } 
        } 
    }
}