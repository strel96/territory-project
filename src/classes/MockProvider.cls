@IsTest
public class MockProvider implements System.StubProvider{


    public Object handleMethodCall(Object stubbedObject, String stubbedMethodName,
            Type returnType, List<Type> listOfParamTypes, List<String> listOfParamNames,
            List<Object> listOfArgs) {

        if(stubbedMethodName == 'getAccountsId'){
            return TerritoryTestFactory.getAccountsIds(5);
        }




        return null;
    }
}