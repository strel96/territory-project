@IsTest
public with sharing class SharingServiceTest {
    private static SharingService sharingService = new SharingService();
    static Set<Id> accountsIds = TerritoryTestFactory.getAccountsIds(1);
    static Set<Id> usersIds = TerritoryTestFactory.getUsersIds();
    static Set<Id> contactsIds = TerritoryTestFactory.getContactsIds(2);

    @IsTest public static void insertAndDeleteAccountSharing(){
        List<AccountShare> insertedAccountShares = new List<AccountShare>();
        List<AccountShare> deletedAccountShares = new List<AccountShare>();

        Test.startTest();
        sharingService.insertAccountSharing(accountsIds, usersIds);
        insertedAccountShares = [
                SELECT AccountId
                FROM AccountShare
                WHERE AccountId IN :accountsIds
                    AND UserOrGroupId IN :usersIds];

        sharingService.deleteAccountSharing(accountsIds, usersIds);
        deletedAccountShares = [
                SELECT AccountId
                FROM AccountShare
                WHERE AccountId IN :accountsIds
                    AND UserOrGroupId IN :usersIds];
        Test.stopTest();

        System.assert(!insertedAccountShares.isEmpty(), 'Error');
        System.assert(deletedAccountShares.isEmpty(), 'Error');
    }

    @IsTest public static void insertAndDeleteContactSharing(){
        List<ContactShare> insertedContactShares = new List<ContactShare>();
        List<ContactShare> deletedContactShares = new List<ContactShare>();

        Test.startTest();
        sharingService.insertContactSharing(contactsIds, usersIds);
        insertedContactShares = [
                SELECT ContactId
                FROM ContactShare
                WHERE ContactId IN :contactsIds
                    AND UserOrGroupId IN :usersIds];

        sharingService.deleteContactSharing(contactsIds, usersIds);
        deletedContactShares = [
                SELECT ContactId
                FROM ContactShare
                WHERE ContactId IN :contactsIds
                    AND UserOrGroupId IN :usersIds];
        Test.stopTest();

        System.assert(!insertedContactShares.isEmpty(), 'Error');
        System.assert(deletedContactShares.isEmpty(), 'Error');
    }
}