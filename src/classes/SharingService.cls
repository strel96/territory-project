public with sharing class SharingService {
    public void insertAccountSharing(Set<Id> accountsId, Set<Id> usersId) {
        List<AccountShare> accShare = new List<AccountShare>();

        for (Id accountId : accountsId) {
            for (Id userId : usersId) {
                AccountShare newAccountShare = new AccountShare();
                newAccountShare.UserOrGroupId = userId;
                newAccountShare.AccountId = accountId;
                newAccountShare.AccountAccessLevel = 'Edit';
                newAccountShare.OpportunityAccessLevel = 'Edit';
                accShare.add(newAccountShare);
            }
        }
        
        if (!accShare.isEmpty()) upsert accShare;
            
    }

    public void insertContactSharing(Set<Id> contactsId, Set<Id> usersId) {
        List<ContactShare> contactShare = new List<ContactShare>();

        for (Id contactId : contactsId) {
            for (Id userId : usersId) {
                ContactShare newContactShare = new ContactShare();
                newContactShare.UserOrGroupId = userId;
                newContactShare.ContactId = contactId;
                newContactShare.ContactAccessLevel = 'Edit';
                contactShare.add(newContactShare);
            }
        }
        
        if (!contactShare.isEmpty()) upsert contactShare;
            
    }

    public void deleteAccountSharing(Set<Id> accountsId, Set<Id> usersId) {
        List<AccountShare> accShare = [
                SELECT UserOrGroupId, AccountId
                FROM AccountShare
                WHERE AccountId IN :accountsId AND UserOrGroupId IN :usersId
        ];
        
        if (accShare != null) delete accShare;

    }

    public void deleteContactSharing(Set<Id> contactsId, Set<Id> usersId) {
        List<ContactShare> contactShare = [
                SELECT UserOrGroupId, ContactId
                FROM ContactShare
                WHERE ContactId IN :contactsId AND UserOrGroupId IN :usersId
        ];
        
        if (contactShare != null) delete contactShare;
           
        
    }

}