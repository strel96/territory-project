trigger AccountShareTrigger on Account (after insert, after update, after delete) {
    AccountShareTriggerHandler accountShare = new AccountShareTriggerHandler();
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            accountShare.createSharing(Trigger.newMap);
        }
        if(Trigger.isUpdate){
            accountShare.deleteSharing(Trigger.oldMap);
            accountShare.createSharing(Trigger.newMap);
        }
        if(Trigger.isDelete){
            accountShare.deleteSharing(Trigger.oldMap);
        }
    }
}