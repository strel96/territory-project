trigger ReferenceShareTrigger on Reference__c (after insert, after update, after delete) {
   ReferenceShareTriggerHandler referenceShare = new ReferenceShareTriggerHandler();
    
    if(Trigger.isAfter){
      if(Trigger.isInsert){
         referenceShare.createSharing(Trigger.new);
      }
      if(Trigger.isUpdate){
         referenceShare.deleteSharing(Trigger.old);
         referenceShare.createSharing(Trigger.new);
      }
      if(Trigger.isDelete){
         referenceShare.deleteSharing(Trigger.old);
      }
   }
}