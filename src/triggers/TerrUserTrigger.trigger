trigger TerrUserTrigger on TerrUser__c (after insert, after update, after delete) {
    TerrUserShareTriggerHandler terrUserShare = new TerrUserShareTriggerHandler();
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            terrUserShare.createSharing(Trigger.new);
        }
        if(Trigger.isUpdate){
            terrUserShare.deleteSharing(Trigger.old);
            terrUserShare.createSharing(Trigger.new);
        }
        if(Trigger.isDelete){
            terrUserShare.deleteSharing(Trigger.old);
        }
    }
}